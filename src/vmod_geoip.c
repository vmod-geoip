#include <config.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

#include <cache/cache.h>
#include <vcl.h>
#include <vcc_if.h>

#ifdef VPFX
# define VEVENT(a) VPFX(a)
#else
/* For compatibility with varnish prior to 6.2 */
# define VEVENT(a) a
#endif

#include <vtcp.h>
#include <vsa.h>

#include <maxminddb.h>
#include <assert.h>

#define DEFAULT_DIR  "/usr/share/GeoIP"
#define DEFAULT_FILE "GeoLite2-City.mmdb"
#define DEFAULT_DATABASE DEFAULT_DIR "/" DEFAULT_FILE

struct geoip_config {
	char *database;
};

#if (VARNISHAPI_MAJOR == 6 && VARNISHAPI_MINOR >= 6) || VARNISHAPI_MAJOR > 6
static void
free_conf(VRT_CTX, void *data)
{
	free(data);
}

static const struct vmod_priv_methods free_conf_methods[1] = {{
                .magic = VMOD_PRIV_METHODS_MAGIC,
                .type = "vmod_geoip",
                .fini = free_conf
}};
#else
static void
free_conf(void *data)
{
	struct geoip_config *cfg = data;
	free(data);
}
#endif

int
VEVENT(geoip_event)(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
        if (e == VCL_EVENT_LOAD) {
                struct geoip_config *conf = calloc(1, sizeof(*conf));
                AN(conf);
                priv->priv = conf;
#if (VARNISHAPI_MAJOR == 6 && VARNISHAPI_MINOR >= 6) || VARNISHAPI_MAJOR > 6
		priv->methods = free_conf_methods;
#else		
		priv->free = free_conf;
#endif
        }
        return 0;
}

void
vmod_init(VRT_CTX, struct vmod_priv *priv, const char *dir)
{
	struct geoip_config *conf = priv->priv;
	struct stat st;
	char *name;
	
	if (stat(dir, &st)) {
		VSLb(ctx->vsl, SLT_Error,
		     "geoip.init: can't stat \"%s\": %s",
		     dir, strerror(errno));
		abort();
	}

	if (S_ISDIR(st.st_mode)) {
		size_t dlen = strlen (dir);
		size_t len;
		
		if (dir[dlen-1] == '/')
			dlen--;
		name = malloc (dlen + 1 + sizeof (DEFAULT_FILE));
		AN(name);
		memcpy (name, dir, dlen);
		name[dlen++] = '/';
		strcpy (name + dlen, DEFAULT_FILE);
	} else if (S_ISREG (st.st_mode)) {
		name = strdup (dir);
		AN(name);
	} else {
		VSLb(ctx->vsl, SLT_Error,
		      "geoip.init: \"%s\": bad file type", dir);
		abort ();
	}
		
	free(conf->database);
	conf->database = name;
}

static int
open_geoip_database(VRT_CTX, struct geoip_config *conf, MMDB_s *dbptr)
{
	char *database = conf->database ? conf->database : DEFAULT_DATABASE;
	int rc;
	
	rc = MMDB_open(database, MMDB_MODE_MMAP, dbptr);
	if (rc != MMDB_SUCCESS) {
		VSLb(ctx->vsl, SLT_Error,
		      "can't open database \"%s\": %s",
		      database, MMDB_strerror(rc));
		return -1;
	}
	return 0;
}

static const char *
conv_utf_string (struct ws *ws, MMDB_entry_data_s *dptr)
{
	char *retval = WS_Alloc(ws, dptr->data_size + 1);
	AN(retval);
	memcpy(retval, dptr->utf8_string, dptr->data_size);
	retval[dptr->data_size] = 0;
	return retval;
}

static const char *
conv_uint16(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%" PRIu16, dptr->uint16);
}

static const char *
conv_uint32(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%" PRIu32, dptr->uint32);
}

static const char *
conv_int32(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%" PRIi32, dptr->int32);
}

static const char *
conv_bool(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%01d", dptr->boolean ? 1 : 0);
}

static const char *
conv_double(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%g", dptr->double_value);
}

static const char *
conv_float(struct ws *ws, MMDB_entry_data_s *dptr)
{
	return WS_Printf(ws, "%g", dptr->float_value);
}

static const char *(*entry_conv[]) (struct ws *, MMDB_entry_data_s *) = {
	[MMDB_DATA_TYPE_UTF8_STRING] = conv_utf_string,
	[MMDB_DATA_TYPE_UINT16]      = conv_uint16,
	[MMDB_DATA_TYPE_UINT32]      = conv_uint32,
	[MMDB_DATA_TYPE_INT32]       = conv_int32,
	[MMDB_DATA_TYPE_BOOLEAN]     = conv_bool,
	[MMDB_DATA_TYPE_DOUBLE]      = conv_double,
	[MMDB_DATA_TYPE_FLOAT]       = conv_float
};

static const char *
lookup_geoip_database(VRT_CTX,
		      MMDB_s *dbfile, VCL_IP ip,
		      char const *pathstr, char **path)
{
	MMDB_lookup_result_s result;
	int mmdb_error;
	int rc;
	MMDB_entry_data_s entry_data;
	const char *retval;
	const struct sockaddr *sa;
	socklen_t sl;
	char ipstr[VTCP_ADDRBUFSIZE];
	
	if (!(sa = VSA_Get_Sockaddr(ip, &sl)))
		return NULL;

	result = MMDB_lookup_sockaddr(dbfile, sa, &mmdb_error);
	if (mmdb_error != MMDB_SUCCESS) {
		VTCP_name(ip, ipstr, sizeof ipstr, NULL, 0);
		VSLb(ctx->vsl, SLT_Error,
		     "%s: %s",
		     pathstr, ipstr, MMDB_strerror(mmdb_error));
		return NULL;
	}

	if (!result.found_entry)
		return NULL;
	
	rc = MMDB_aget_value(&result.entry, &entry_data,
			     (const char * const* const) path);
	if (rc != MMDB_SUCCESS) {
		VTCP_name(ip, ipstr, sizeof ipstr, NULL, 0);
		VSLb(ctx->vsl, SLT_Error,
		     "%s %s: MMDB_aget_value %s",
		     pathstr, ipstr, MMDB_strerror(rc));
		return NULL;
	}

	if (!entry_data.has_data)
		return NULL;

	if (entry_data.type >= 0
	    && entry_data.type <= sizeof (entry_conv) / sizeof (entry_conv[0])
	    && entry_conv[entry_data.type]) {
		retval = entry_conv[entry_data.type] (ctx->ws, &entry_data);
	} else {
		VTCP_name(ip, ipstr, sizeof ipstr, NULL, 0);
		retval = WS_Printf(ctx->ws, "[%s: can't format %s of type %d]",
				   ipstr, pathstr, entry_data.type);
	}
	return retval;
}

static char **
split(char const *path)
{
	int c = 0;
	int i, j;
	char *base;
	char **retv;
	char *str;

	c = 1;
	for (i = 0; path[i]; i++)
		if (path[i] == '.')
			c++;
	base = malloc(i + 1 + (c + 1) * sizeof(char*));
	AN(base);
	retv = (char**) base;
	str  = (char*) &retv[c+1];
	strcpy(str, path);

	i = 0;
	j = 0;
	retv[j++] = str;
	while (str[i]) {
		if (str[i] == '.') {
			str[i] = 0;
			retv[j++] = str + i + 1;
		}
		i++;
	}
	retv[j] = NULL;
	return retv;
}

VCL_STRING
vmod_get(VRT_CTX, struct vmod_priv *priv, VCL_IP ip, VCL_STRING path)
{
	struct geoip_config *conf = priv->priv;
	MMDB_s dbfile;
	const char *retval = NULL;
	char **pathv;

	if (open_geoip_database(ctx, conf, &dbfile))
		return NULL;
	pathv = split(path);
	retval = lookup_geoip_database(ctx, &dbfile, ip, path, pathv);
	free(pathv);
	MMDB_close(&dbfile);
	return retval;
}

VCL_STRING
vmod_country_code(VRT_CTX, struct vmod_priv *priv, VCL_IP ip)
{
	struct geoip_config *conf = priv->priv;
	MMDB_s dbfile;
	char const *retval = NULL;
	static char *country_code_path[] = {
		"country",
		"iso_code",
		NULL
	};
	
	if (open_geoip_database(ctx, conf, &dbfile))
		return NULL;
	retval = lookup_geoip_database(ctx, &dbfile, ip,
				       "country.iso_code", country_code_path);
	MMDB_close(&dbfile);
	return retval;
}

VCL_STRING
vmod_country_name(VRT_CTX, struct vmod_priv *priv, VCL_IP ip)
{
	struct geoip_config *conf = priv->priv;
	MMDB_s dbfile;
	const char *retval = NULL;
	static char *country_name_path[] = {
		"country",
		"names",
		"en",
		NULL
	};

	if (open_geoip_database(ctx, conf, &dbfile))
		return NULL;
	retval = lookup_geoip_database(ctx, &dbfile, ip,
				       "country.names.en",
				       country_name_path);
	MMDB_close(&dbfile);
	return retval;
}

VCL_STRING
vmod_city_name(VRT_CTX, struct vmod_priv *priv, VCL_IP ip)
{
	struct geoip_config *conf = priv->priv;
	MMDB_s dbfile;
	const char *retval = NULL;
	static char *city_name_path[] = {
		"city",
		"names",
		"en",
		NULL
	};
	
	if (open_geoip_database(ctx, conf, &dbfile))
		return NULL;
	retval = lookup_geoip_database(ctx, &dbfile, ip,
				       "city.names.en",
				       city_name_path);
	MMDB_close(&dbfile);
	return retval;
}
